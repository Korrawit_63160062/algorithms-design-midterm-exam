package com.mycompany.algorithmsdesignmidterm;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Scanner;
import java.util.stream.Stream;

/**
 *
 * @author Acer
 */
class A1_10 {

    public static void longestSortedSubarray(int[] A, int size) {

        int max = 1, countMax = 1, index = 0;

        for (int i = 1; i < size; i++) {

            if (A[i] > A[i - 1]) {
                countMax++;
            } else {

                if (max < countMax) {
                    max = countMax;

                    index = i - max;
                }

                countMax = 1;
            }
        }

        if (max < countMax) {
            max = countMax;
            index = size - max;
        }

        for (int i = index; i < max + index; i++) {
            System.out.print(A[i] + " ");
        }
    }

    public static void main(String[] args) throws IOException {
        Scanner kb = new Scanner(System.in);
        int n = kb.nextInt();
        int[] A = new int[n];
        for (int i = 0; i < n; i++) {
            A[i] = kb.nextInt();
        }
        longestSortedSubarray(A, A.length);
    }
}
