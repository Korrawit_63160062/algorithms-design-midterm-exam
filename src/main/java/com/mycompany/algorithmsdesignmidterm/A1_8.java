/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.algorithmsdesignmidterm;

import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author Acer
 */
public class A1_8 {

    public static int[] reverseArr(int[] A, int size) {

        for (int i = 0; i < size / 2; i++) {
            int temp = A[size - 1 - i];
            A[size - 1 - i] = A[i];
            A[i] = temp;
        }
        return A;
    }

    private static void printArr(int[] A, int size) {
        for (int i = 0; i < size; i++) {
            System.out.print(A[i] + " ");
        }
    }

    public static void main(String[] args) throws IOException {
        Scanner kb = new Scanner(System.in);
        int n = kb.nextInt();
        int[] A = new int[n];
        for (int i = 0; i < n; i++) {
            A[i] = kb.nextInt();
        }
        A = reverseArr(A, A.length);
        printArr(A, A.length);
    }

}
